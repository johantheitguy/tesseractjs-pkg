// Copied from the basic example in tesseract.js
// Modified to use a jpeg

var path = require('path');
var Tesseract = require('tesseract.js');
var image = path.resolve(__dirname, 'hello.jpg');

Tesseract.recognize(image)
.then(data => {
	console.log('then\n', data.text)
})
.catch(err => {
  console.log('catch\n', err);
})
.finally(e => {
  console.log('finally\n');
});